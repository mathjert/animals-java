import Animals.AnimalMovementEnum;
import MyAnimals.Cat;
import MyAnimals.Crocodile;
import MyAnimals.Human;
import MyAnimals.Parrot;

import static java.util.Arrays.asList;

public class Main {
    public static void main(String[] args) {
        Cat tom = new Cat("Tom");
        tom.showMovements();
        tom.chooseRandomMovement();

        Human hum = new Human("Trcc My");
        hum.showMovements();
        hum.chooseRandomMovement();

        Parrot p = new Parrot("Pirate bird");
        p.showMovements();
        p.chooseRandomMovement();

        Human mat = new Human("Mathias Hjertholm");
        mat.showMovements();
        mat.chooseRandomMovement();// adding a commet here

        // testing a different setup
        Crocodile croc = new Crocodile("Stewart", AnimalMovementEnum.SWIM, AnimalMovementEnum.RUN, AnimalMovementEnum.WALK);
        croc.showMovements();
        croc.chooseRandomMovement();
        croc.eatMeat();
    }
}
