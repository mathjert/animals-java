package Animals;

public enum AnimalMovementEnum {
    RUN("run"),
    WALK("walk"),
    FLY("fly"),
    CLIMB("climb"),
    SWIM("swim");

    private String movement;

    private AnimalMovementEnum(String movements){
        this.movement = movements;
    }

    public String toString(){
        return this.movement;
    }
}
