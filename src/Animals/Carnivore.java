package Animals;

public abstract class Carnivore extends Animal implements iCarnivore{

    public Carnivore (){
        super(AnimalType.CARNIVORE);
    }
    public Carnivore(AnimalMovementEnum ...movements){
        super(AnimalType.CARNIVORE, movements);
    }
    public Carnivore(String name, AnimalMovementEnum ...movements){
        super(name, AnimalType.CARNIVORE, movements);
    }
    public Carnivore(AnimalRace race , AnimalMovementEnum ...movements){
        super(race, AnimalType.CARNIVORE, movements);
    }
    public Carnivore(String name, AnimalRace race, AnimalMovementEnum ...movements){
        super(name, race, AnimalType.CARNIVORE, movements);
    }

    // carnivore interface methods
    @Override
    public void eatMeat() {
        // eat meat
        System.out.println(this.name + " " + "eats meat");
    }
}
