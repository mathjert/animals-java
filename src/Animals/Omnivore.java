package Animals;

public class Omnivore extends Animal implements iCarnivore, iHerbivore{

    public Omnivore (){
        super(AnimalType.UNDEFINED);
    }
    public Omnivore(AnimalType type, AnimalMovementEnum ...movements){
        super(type, movements);
    }
    public Omnivore(String name, AnimalMovementEnum ...movements){
        super(name, AnimalType.OMNIVORE, movements);
    }
    public Omnivore(AnimalRace race , AnimalMovementEnum ...movements){
        super(race, AnimalType.OMNIVORE, movements);
    }
    public Omnivore(String name, AnimalRace race, AnimalMovementEnum ...movements){
        super(name, race, AnimalType.OMNIVORE, movements);
    }

    // carnivore interface requirements
    @Override
    public void eatMeat() {
        // eat meat
        System.out.println(this.name + " " + "eats meat");
    }

    // herbivore interface requirements
    @Override
    public void eatVegan() {
        // eat plants
        System.out.println(this.name + " " + "eats plants");
    }
}
