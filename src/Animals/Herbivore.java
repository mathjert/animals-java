package Animals;

public class Herbivore extends Animal implements iHerbivore{

    public Herbivore (){
        super(AnimalType.UNDEFINED);
    }
    public Herbivore(AnimalType type, AnimalMovementEnum...animals){
        super(type, animals);
    }
    public Herbivore(String name, AnimalMovementEnum...movements){
        super(name, AnimalType.HERBIVORE, movements);
    }
    public Herbivore(AnimalRace race , AnimalMovementEnum...movements){
        super(race, AnimalType.HERBIVORE, movements);
    }
    public Herbivore(String name, AnimalRace race, AnimalMovementEnum...movements){
        super(name, race, AnimalType.HERBIVORE, movements);
    }

    // herbivore interface methods
    @Override
    public void eatVegan() {
        // eat plants
        System.out.println(this.name + " " + "eats plants");
    }
}
