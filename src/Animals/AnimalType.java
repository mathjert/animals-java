package Animals;

public enum AnimalType {
    HERBIVORE,
    CARNIVORE,
    OMNIVORE,
    UNDEFINED
}
