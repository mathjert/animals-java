package Animals;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.util.Arrays.asList;

abstract class Animal implements iAnimalMovements{
    protected String name;
    protected AnimalType type;
    protected AnimalRace race;
    protected List<AnimalMovementEnum> movements;

    Animal(AnimalType type, AnimalMovementEnum... movements){
        this.name = "UNKNOWN";
        this.type = type;
        this.race = AnimalRace.UNKNWOWN;
        if(movements.length > 0)
            this.movements = asList(movements);
    }
    Animal(String name, AnimalType type, AnimalMovementEnum... movements){
        this.name = name;
        this.race = AnimalRace.UNKNWOWN;
        this.type = type;
        if(movements.length > 0)
            this.movements = asList(movements);
    }
    Animal(AnimalRace race, AnimalType type, AnimalMovementEnum... movements){
        this.name = "UNKNOWN";
        this.race = race;
        this.type = type;
        if(movements.length > 0)
            this.movements = asList(movements);
    }
    Animal(String name, AnimalRace race, AnimalType type, AnimalMovementEnum... movements){
        this.name = name;
        this.race = race;
        this.type = type;
        if(movements.length > 0)
            this.movements = asList(movements);
    }

    //movements.
    private void run(){
        if(movements.contains(AnimalMovementEnum.RUN))
            System.out.println("I am running");
    }
    private void fly(){
        if(movements.contains(AnimalMovementEnum.FLY))
            System.out.println("I am flying");
    }
    private void walk(){
        if(movements.contains(AnimalMovementEnum.WALK))
            System.out.println("Hey! I'm walkin here");
    }
    private void swim(){
        if(movements.contains(AnimalMovementEnum.SWIM))
            System.out.println("I am swimming");
    }
    private void climb(){
        if(movements.contains(AnimalMovementEnum.CLIMB))
            System.out.println("I am climbing");
    }

    //getters
    public AnimalRace getRace() {
        return race;
    }

    public AnimalType getType() {
        return type;
    }

    public List<AnimalMovementEnum> getMovements() {
        return movements;
    }

    public String getName() {
        return name;
    }

    //setters

    //diverging a bit from JavaBeans standard due to the nature of the system
    public void appendMovements(AnimalMovementEnum movement) {
        if(!this.movements.contains(movement))
            this.movements.add(movement);
        //this.movements.add(movement -> !this.movements.contains(movement)); didn't work ...
    }

    public void setName(String name) {
        this.name = name;
    }


    // movement selection
    public void showMovements(){
        StringBuilder sb = new StringBuilder();
        int count = 0;

        // i have no gender of these animals, so it seems the most appropriate
        sb.append("the ").append(race.toString()).append(" ").append(name).append(" says it can: ");
        for(AnimalMovementEnum e : movements){
            if(++count == movements.size())
                sb.append("and ");

            sb.append(e.toString()).append(" ");
        }
        System.out.println(sb.toString());
    }
    public void chooseRandomMovement(){
        int rand = ThreadLocalRandom.current().nextInt(0, movements.size());
        AnimalMovementEnum movement = movements.get(rand);
        System.out.print("The " + race.toString() + " Tom says: ");
        switch (movement){
            case FLY:
                fly();
                break;
            case RUN:
                run();
                break;
            case SWIM:
                swim();
                break;
            case WALK:
                walk();
                break;
            case CLIMB:
                climb();
                break;
            default:
                System.out.println("Unknown movement");
                break;
        }
    }
}
