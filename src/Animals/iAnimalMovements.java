package Animals;

public interface iAnimalMovements {
    void showMovements();
    void chooseRandomMovement();
}
