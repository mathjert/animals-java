package Animals;

public enum AnimalRace {
    CAT("cat"),
    DOG("dog"),
    COW("cow"),
    HUMAN("human"),
    PIG("pig"),
    PARROT("parrot"),
    CROCODILE("crocodile"),
    SHEEP("sheep"),
    UNKNWOWN("UNKNOWN");

    private String race;

    private AnimalRace(String races){
        this.race = races;
    }

    public String toString(){
        return this.race;
    }

}
