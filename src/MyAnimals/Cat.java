package MyAnimals;

import Animals.AnimalMovementEnum;
import Animals.AnimalRace;
import Animals.Carnivore;

public class Cat extends Carnivore {
    public Cat(String name){
        super(name, AnimalRace.CAT, AnimalMovementEnum.RUN, AnimalMovementEnum.WALK, AnimalMovementEnum.SWIM, AnimalMovementEnum.CLIMB);
    }
}
