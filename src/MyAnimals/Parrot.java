package MyAnimals;

import Animals.AnimalMovementEnum;
import Animals.AnimalRace;
import Animals.Omnivore;

public class Parrot extends Omnivore {
    public Parrot(String name){
        super(name, AnimalRace.PARROT, AnimalMovementEnum.FLY, AnimalMovementEnum.SWIM);
    }
}
