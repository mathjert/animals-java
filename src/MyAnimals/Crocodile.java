package MyAnimals;

import Animals.AnimalMovementEnum;
import Animals.AnimalRace;
import Animals.Carnivore;

public class Crocodile extends Carnivore {
    public Crocodile(String name, AnimalMovementEnum ...movements){
        super(name, AnimalRace.CROCODILE, movements);
    }
}
