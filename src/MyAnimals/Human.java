package MyAnimals;

import Animals.AnimalMovementEnum;
import Animals.AnimalRace;
import Animals.Omnivore;


public class Human extends Omnivore {
    public Human(String name){
        super(name, AnimalRace.HUMAN, AnimalMovementEnum.WALK, AnimalMovementEnum.RUN, AnimalMovementEnum.SWIM, AnimalMovementEnum.CLIMB);
    }
}
